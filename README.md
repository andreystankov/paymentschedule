# Simple library that will generate payment schedule for credit loan


## Description
Library that will generate payment schedule for credit loan.

## Input params
1. Number of installments - allowed [3-24] number of installments.
2. Credit amount - allowed [500-5000] leva.
3. Annual interest rate - persent (10 means 10%).
4. Maturity date - allowed days are 10,20 or end of month in format (2021-03-31).
5. Credit start date in format (2021-03-16)
6. Additional taxes - array with a tax name and a tax amount. Taxes are distributed equaly among all installments.

**Input example**
```array
$params = [
	'numberOfInstallments' => 3,
	'amount' => 500,
	'air' => 10,
	'maturityDate' => '2021-03-31',
	'utilisationDate' => '2021-03-16',
	'taxes' => [
		'tax1' => 25,
		'tax2' => 17'
	]
];
```

## Output params
1. Payment schedule with annuity installments

**Output example**
```array
[
     0 => [
	    'number' => 1,
	    'date' => '2021-03-31',
		'period' => 16,
		'installmentAmount' => 183.45,
		'principal' => 165.29,
		'interest' => 4.16,
		'tax1' => 8.33,
		'tax2' => 5.67,
	 ],
     1 => [
	    'number' => 2,
	    'date' => '2021-04-30',
		'period' => 30,
		'installmentAmount' => 183.45,
		'principal' => 166.66,
		'interest' => 2.79,
		'tax1' => 8.33,
		'tax2' => 5.67,
	 ],
     2 => [
	    'number' => 3,
	    'date' => '2021-05-31',
		'period' => 31,
		'installmentAmount' => 183.45,
		'principal' => 168.05,
		'interest' => 1.40,
		'tax1' => 8.34,
		'tax2' => 5.66,
	 ], 
]
```

All differences from roundings go to last installment.

---

## Installation instructions
```
composer require andreystankov/paymentschedule:dev-master
```
## Run Test

* Navigate to '/vendor/andreystankov/paymentschedule/tests' in
the root of your project folder
* Edit the input array
* And run test
```
php test.php
```

## Authors

Andrey Stankov
andrey.stankov@gmail.com