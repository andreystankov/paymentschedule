<?php

require_once __DIR__ . '/../../../../vendor/autoload.php';

use Andreystankov\PaymentSchedule\Controller\PaymentScheduleController;

$test = new PaymentScheduleController();
$params = [
    'numberOfInstallments' => 3,
    'amount' => 5000,
    'air' => 10,
    'maturityDate' => '2021-03-10',
    'utilisationDate' => '2021-03-16',
    'taxes' => [
        'tax1' => 25,
        'tax2' => 17,
        // 'anotherTax' => 15
    ]
];
print_r($test->indexAction($params));
