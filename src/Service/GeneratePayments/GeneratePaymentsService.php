<?php

namespace Andreystankov\PaymentSchedule\Service\GeneratePayments;

use Andreystankov\PaymentSchedule\Service\GeneratePayments\GeneratePaymentsServiceInterface;
use DateTime;

class GeneratePaymentsService implements GeneratePaymentsServiceInterface
{

    public function calcMonthAmount($params)
    {

        $months = $params['numberOfInstallments'];
        $interest = $params['air'] / 1200;
        $loan = $params['amount'];
        $amount = $interest * -$loan * pow((1 + $interest), $months) / (1 - pow((1 + $interest), $months));
        return round($amount, 2);
    }

    public function calculation($params)
    {
        $taxesCalc = $this->taxCalc($params);
        $monthAmount = $this->calcMonthAmount($params);

        $balans = $params['amount'];
        $paymentDate = $params['maturityDate'];

        if ($this->checkOverlapDays($params['utilisationDate'], $paymentDate) === true) {
            $paymentDate = $this->nextPaymentDate($paymentDate);
        }
        $prevDate = $params['utilisationDate'];

        for ($i = 0; $i <= $params['numberOfInstallments'] - 1; $i++) {
            $interest = $this->calcInterest($balans, $params['air']);
            $principal = round(($monthAmount - $interest), 2);

            $result[$i] = [
                'number' => $i + 1,
                'date' => $paymentDate,
                'period' => $this->dateDiff($prevDate, $paymentDate),
                'installmentAmount' => ($principal + $interest + $taxesCalc['monthTaxSum'][$i]),
                'principal' => $principal,
                'interest' => $interest
            ];
            foreach ($taxesCalc['params'][$i] as $taxName => $taxAmount) {
                $result[$i][$taxName] = $taxAmount;
            }
            $balans = $balans - $principal;
            $prevDate = $paymentDate;
            $paymentDate = $this->nextPaymentDate($paymentDate);
        }
        return $result;
    }

    public function calcInterest($balans, $air)
    {
        return round($balans * (($air / 100) / 12), 2);
    }

    public function taxCalc($params)
    {
        $taxArray = $params['taxes'];
        $months = $params['numberOfInstallments'];
        foreach ($taxArray as $taxName => $taxAmount) {

            $tax = round($taxAmount / $months, 2);
            for ($i = 0; $i <= $months - 1; $i++) {
                $result[$i][$taxName] = $tax;
                $taxAmount = $taxAmount - $tax;

                if ($i == $months - 1) {
                    $result[$i][$taxName] = round($tax + $taxAmount,2);
                }
            }
        }
        $monthTaxSum = [];
        for ($i = 0; $i <= $months - 1; $i++) {
            foreach ($result[$i] as $key => $value) {
                $monthTaxSum[$i] = $monthTaxSum[$i] + $value;
            }
        }
        return array('params' => $result, 'monthTaxSum' => $monthTaxSum);
    }

    public function dateDiff($firstDate, $secoundDate)
    {

        return (new DateTime($firstDate))->diff(new DateTime($secoundDate))->days;
    }

    public function nextPaymentDate($prevDate)
    {
        if (date("d", strtotime($prevDate)) == 10 || date("d", strtotime($prevDate)) == 20) {
            $date = new DateTime($prevDate);
            $date->modify('+1 month');
            return $date->format('Y-m-d');
        }
        if (date("t", strtotime($prevDate)) == date("d", strtotime($prevDate))) {
            $date = new DateTime($prevDate);
            $date->modify('first day of next month');
            return $date->format('Y-m-t');
        }
    }

    public function checkOverlapDays($firstDate, $secoundDate)
    {
        if (date("d", strtotime($firstDate)) >= date("d", strtotime($secoundDate))) {
            return true;
        } else {
            return false;
        }
    }
}
