<?php

namespace Andreystankov\PaymentSchedule\Service\GeneratePayments;

interface GeneratePaymentsServiceInterface
{
    public function calcMonthAmount($params);
    public function calculation($params);
    public function calcInterest($balans, $air);
    public function taxCalc($params);
    public function dateDiff($firstDate, $secoundDate);
    public function nextPaymentDate($prevDate);
    public function checkOverlapDays($firstDate, $secoundDate);

}