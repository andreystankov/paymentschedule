<?php

namespace Andreystankov\PaymentSchedule\Service\InputValidation;

interface InputValidationServiceInterface
{
    public function checkConsistency($params);
    public function checkNumInstallments($params);
    public function checkAllowedAmount($params);
    public function checkAIR($params);
    public function checkMaturityDate($params);
    public function checkUtilisationDate($params);
    public function validateDate($date, $format = 'Y-m-d');
    public function validateDay($date);
    public function checkTaxArray($params);
}