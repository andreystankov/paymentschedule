<?php

namespace Andreystankov\PaymentSchedule\Service\InputValidation;

use Andreystankov\PaymentSchedule\Service\InputValidation\InputValidationServiceInterface;
use DateTime;

class InputValidationService implements InputValidationServiceInterface
{

    public function checkConsistency($params)
    {
        $params = $this->checkNumInstallments($params);

        $params = $this->checkAllowedAmount($params);

        $params = $this->checkAIR($params);

        $params = $this->checkMaturityDate($params);

        $params = $this->checkUtilisationDate($params);

        $params = $this->checkTaxArray($params);
        $error = [];
        foreach ($params as $key => $value) {

            if ($value !== true) {
                $error[$key] = $value;
            }
        }
        if (count($error) == 0) {
            return true;
        } else {
            return $error;
        }
    }

    public function checkNumInstallments($params)
    {
        if ($params['numberOfInstallments'] >= 3 && $params['numberOfInstallments'] <= 24) {
            $params['numberOfInstallments'] = true;
        } else {
            $params['numberOfInstallments'] = 'Allowed installments are between 3-24';
        }
        return $params;
    }

    public function checkAllowedAmount($params)
    {
        if ($params['amount'] >= 500 && $params['amount'] <= 5000) {
            $params['amount'] = true;
        } else {
            $params['amount'] = 'Allowed amount is between 500-5000';
        }
        return $params;
    }
    public function checkAIR($params)
    {
        if (is_numeric($params['air'])) {
            $params['air'] = true;
        } else {
            $params['air'] = 'Allowed input is number';
        }
        return $params;
    }

    public function checkMaturityDate($params)
    {
        if (
            $this->validateDate($params['maturityDate']) &&
            $this->validateDay($params['maturityDate'])
        ) {
            $params['maturityDate'] = true;
        } else {
            $params['maturityDate'] = 'Wrong Date or format. Use Y-m-d. Alowed days are: 10,20 or EOM';
        }
        return $params;
    }

    public function checkUtilisationDate($params)
    {
        if ($this->validateDate($params['utilisationDate'])) {
            $params['utilisationDate'] = true;
        } else {
            $params['utilisationDate'] = 'Wrong Date or format. Use Y-m-d';
        }
        return $params;
    }

    public function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function validateDay($date)
    {
        if (
            date("t", strtotime($date)) == date("d", strtotime($date)) ||
            date("d", strtotime($date)) == 10 ||
            date("d", strtotime($date)) == 20

        ) {
            return true;
        }
    }

    public function checkTaxArray($params)
    {
        if (is_array($params['taxes'])) {
            foreach ($params['taxes'] as $value) {
                if (!is_numeric($value)) {
                    $params['taxes'] = 'Allowed input is number.';
                    return $params;
                }
            }
            $params['taxes'] = true;
        } else {
            $params['taxes'] = 'Allowed input is array with a tax name and a tax amount.';
        }
        return $params;
    }
}
