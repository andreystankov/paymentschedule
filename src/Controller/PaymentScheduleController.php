<?php 

namespace Andreystankov\PaymentSchedule\Controller;

use Andreystankov\PaymentSchedule\Service\GeneratePayments\GeneratePaymentsService;
use Andreystankov\PaymentSchedule\Service\InputValidation\InputValidationService;

class PaymentScheduleController 
{

    public function __construct()
    {
        $this->inputValidationService = new InputValidationService();
        $this->generatePaymentsService = new GeneratePaymentsService();
    }
 
    public function indexAction($params){
        $originalParams=$params;
        $result = $this->inputValidationService->checkConsistency($params);
        if($result !== true){
            return $result;
        }else{
           $result = $this->generatePaymentsService->calculation($params);
        }
       return $result;
    }




}